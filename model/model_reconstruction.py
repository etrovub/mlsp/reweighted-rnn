import torch
import torch.nn.functional as functional
from os.path import join, isfile
from datetime import datetime
import os
import numpy as np


class Unfolding_RNN(torch.nn.Module):
    def __init__(self, A_initializer, D_initializer, config):
        super(Unfolding_RNN, self).__init__()
        self.config = config
        self.n_input = int(self.config['n_features'] / self.config['compression_factor'])
        self.dtype = torch.float32
        if torch.cuda.is_available():
            self.device = torch.device('cuda')
        else:
            self.device = torch.device('cpu')
        
        # model parameters
        self.D = torch.nn.Parameter(torch.Tensor(D_initializer))
        self.A = torch.nn.Parameter(torch.Tensor(A_initializer))
        self.alpha = torch.nn.Parameter(torch.tensor(self.config['alpha']))
        self.lambda0 = torch.nn.Parameter(torch.tensor(self.config['lambda0']))
        self.lambda1 = torch.nn.Parameter(torch.tensor(self.config['lambda1']))
        self.lambda2 = torch.nn.Parameter(torch.tensor(self.config['lambda2']))
        self.h_0 = torch.nn.Parameter(torch.zeros(self.config['n_hidden']))

        if not self.config['learn_lambda0']:
            self.lambda0.requires_grad = False
        if self.config['model'].lower() in ['l1l1', 'reweighted']:
            self.G = torch.nn.Parameter(torch.eye(self.config['n_hidden']))
            
            if self.config['model'].lower() == 'reweighted':
                self.g = torch.nn.Parameter(torch.ones(self.config['K'], self.config['n_hidden'], dtype=self.dtype))

                self.Z = torch.nn.Parameter(torch.Tensor(np.tile(np.eye(self.config['n_hidden']), [self.config['K'], 1, 1])))

        elif self.config['model'] == 'gru':
            self.compression = torch.nn.Linear(config['n_features'], self.n_input, bias = False)
            self.reconstruction = torch.nn.Linear(config['n_hidden'], config['n_features'], bias=False)
            self.generic = torch.nn.GRU(self.n_input, self.config['n_hidden'], self.config['K'], batch_first=False)
        elif self.config['model'] == 'rnn':
            self.compression = torch.nn.Linear(config['n_features'], self.n_input, bias = False)
            self.reconstruction = torch.nn.Linear(config['n_hidden'], config['n_features'], bias=False)
            self.generic = torch.nn.RNN(self.n_input, self.config['n_hidden'], self.config['K'], batch_first=False)
        elif self.config['model'] == 'lstm':
            self.compression = torch.nn.Linear(config['n_features'], self.n_input, bias = False)
            self.reconstruction = torch.nn.Linear(config['n_hidden'], config['n_features'], bias=False)
            self.generic = torch.nn.LSTM(self.n_input, self.config['n_hidden'], self.config['K'], batch_first=False)


        if not self.config['learn_lambda1']:
            self.lambda1.requires_grad = False
        if self.config['model'].lower() == 'sista':
            self.F = torch.nn.Parameter(torch.eye(self.config['n_features'], dtype=self.dtype))
        if not self.config['learn_lambda2']:
            self.lambda2.requires_grad = False

    def set_sequence_length(self, time_steps):
        self.config['time_steps'] = time_steps

    def soft_l1(self, x, b):
        out = torch.sign(x) * functional.relu(torch.abs(x) - b)
        return out

    def soft_l1_l1(self, x, w0, w1, alpha1):
        alpha0 = torch.zeros(alpha1.size(), device=self.device, dtype=self.dtype)
        condition = alpha0 <= alpha1
        alpha0_sorted = torch.where(condition, alpha0, alpha1)
        alpha1_sorted = torch.where(condition, alpha1, alpha0)

        w0_sorted = torch.where(condition, w0, w1)
        w1_sorted = torch.where(condition, w1, w0)

        cond1 = x >= alpha1_sorted + w0_sorted + w1_sorted
        cond2 = x >= alpha1_sorted + w0_sorted - w1_sorted
        cond3 = x >= alpha0_sorted + w0_sorted - w1_sorted
        cond4 = x >= alpha0_sorted - w0_sorted - w1_sorted

        res1 = x - w0_sorted - w1_sorted
        res2 = alpha1_sorted
        res3 = x - w0_sorted + w1_sorted
        res4 = alpha0_sorted
        res5 = x + w0_sorted + w1_sorted

        return torch.where(cond1, res1,
                           torch.where(cond2, res2, torch.where(cond3, res3, torch.where(cond4, res4, res5))))

    def soft_l1_l1_reweighted(self, x, w0, w1, alpha1, g, c):
        alpha0 = torch.zeros(alpha1.size(), device=self.device, dtype=self.dtype)
        condition = alpha0 <= alpha1
        alpha0_sorted = torch.where(condition, alpha0, alpha1)
        alpha1_sorted = torch.where(condition, alpha1, alpha0)

        w0_sorted = torch.where(condition, w0, w1) * g
        w1_sorted = torch.where(condition, w1, w0) * g

        x = torch.mm(x, c)

        cond1 = x >= alpha1_sorted + w0_sorted + w1_sorted
        cond2 = x >= alpha1_sorted + w0_sorted - w1_sorted
        cond3 = x >= alpha0_sorted + w0_sorted - w1_sorted
        cond4 = x >= alpha0_sorted - w0_sorted - w1_sorted

        res1 = x - w0_sorted - w1_sorted
        res2 = alpha1_sorted
        res3 = x - w0_sorted + w1_sorted
        res4 = alpha0_sorted
        res5 = x + w0_sorted + w1_sorted

        return torch.where(cond1, res1,
                           torch.where(cond2, res2, torch.where(cond3, res3, torch.where(cond4, res4, res5))))

    def _normalize_compression_matrix(self):
        old_range = self.A.data.max() - self.A.data.min() + 1e-6
        new_range = self.A_range[1] - self.A_range[0]
        self.A.data -= self.A.data.min()
        self.A.data *= new_range/old_range
        self.A.data += self.A_range[0]

    def build_graph_reweighted(self, input):
        At = self.A.t()
        Dt = self.D.t()
        AtA = torch.mm(At, self.A)

        # initialize V
        V = 1.0 / self.alpha * torch.mm(Dt, At)

        # initialize S
        temp = 1. / self.alpha * torch.mm(torch.mm(Dt, AtA), self.D)
        S = torch.eye(self.config['n_hidden'], device=self.device, dtype=self.dtype) - temp

        # initialize W
        W_1 = self.G - torch.mm(temp, self.G)
        # W_k = torch.zeros([self.config['n_hidden'], self.config['n_hidden']], dtype=self.dtype, device=self.device)

        # Hidden layers
        h = []
        h_t_kth_layer = self.h_0.repeat(self.batch_size, 1)

        for t in range(self.config['time_steps']):
            h.append([])
            h_t_last_layer = h_t_kth_layer
            # first ISTA step
            h_t_kth_layer = self.soft_l1_l1_reweighted(torch.mm(h_t_last_layer, W_1.t()) + torch.mm(
                input[t + 1], V.t()), self.lambda0 / self.alpha, self.lambda1 / self.alpha,
                                            torch.mm(h_t_last_layer, self.G), self.g[0], self.Z[0])
            h[-1].append(h_t_kth_layer)
            # 2-k ISTA steps
            for k in range(1, self.config['K']):
                h_t_kth_layer = self.soft_l1_l1_reweighted(torch.mm(input[t + 1], V.t()) + \
                                                    torch.mm(h_t_kth_layer, S.t()),
                                                    self.lambda0 / self.alpha,
                                                    self.lambda1 / self.alpha, torch.mm(h_t_last_layer, self.G), self.g[k], self.Z[k])
                h[-1].append(h_t_kth_layer)

        h_stacked = [torch.stack(h[i]) for i in range(len(h))]
        self.sparse_code = torch.stack(h_stacked)

    def build_graph_l1_l1(self, input):
        At = self.A.t()
        Dt = self.D.t()
        AtA = torch.mm(At, self.A)

        # initialize V
        V = 1.0 / self.alpha * torch.mm(Dt, At)

        # initialize S
        temp = 1. / self.alpha * torch.mm(torch.mm(Dt, AtA), self.D)
        S = torch.eye(self.config['n_hidden'], device=self.device, dtype=self.dtype) - temp

        # initialize W
        W_1 = self.G - torch.mm(temp, self.G)
        # W_k = torch.zeros([self.config['n_hidden'], self.config['n_hidden']], dtype=self.dtype, device=self.device)

        # Hidden layers
        h = []
        h_t_kth_layer = self.h_0.repeat(self.batch_size, 1)

        for t in range(self.config['time_steps']):
            h.append([])
            h_t_last_layer = h_t_kth_layer
            # first ISTA step
            h_t_kth_layer = self.soft_l1_l1(torch.mm(h_t_last_layer, W_1.t()) + torch.mm(
                input[t + 1], V.t()), self.lambda0 / self.alpha, self.lambda1 / self.alpha,
                                            torch.mm(h_t_last_layer, self.G))
            h[-1].append(h_t_kth_layer)
            # 2-k ISTA steps
            for k in range(1, self.config['K']):
                h_t_kth_layer = self.soft_l1_l1(torch.mm(input[t + 1], V.t()) + \
                                                    torch.mm(h_t_kth_layer, S.t()),
                                                    self.lambda0 / self.alpha,
                                                    self.lambda1 / self.alpha, torch.mm(h_t_last_layer, self.G))
                h[-1].append(h_t_kth_layer)

        h_stacked = [torch.stack(h[i]) for i in range(len(h))]
        self.sparse_code = torch.stack(h_stacked)

    def build_graph_sista(self, input):
        At = self.A.t()
        Dt = self.D.t()
        AtA = torch.mm(At, self.A)
        I = torch.eye(self.config['n_features'], device=self.device, dtype=self.dtype)
        P = torch.mm(torch.mm(Dt, self.F), self.D)

        # initialize V
        V = 1.0 / self.alpha * torch.mm(Dt, At)

        # initialize S
        temp = 1. / self.alpha * torch.mm(torch.mm(Dt, AtA + self.lambda2 * I), self.D)
        S = torch.eye(self.config['n_hidden'], device=self.device, dtype=self.dtype) - temp

        # initialize W
        W_1 = (self.alpha + self.lambda2) / self.alpha * P - torch.mm(temp, P)
        W_k = self.lambda2 / self.alpha * P

        # Hidden layers
        h = []
        h_t_kth_layer = self.h_0.repeat(self.batch_size, 1)

        for t in range(self.config['time_steps']):
            h.append([])
            h_t_last_layer = h_t_kth_layer
            # first ISTA step
            h_t_kth_layer = self.soft_l1(torch.mm(h_t_last_layer, W_1.t()) + torch.mm(
                input[t + 1], V.t()), self.lambda0 / self.alpha)
            h[-1].append(h_t_kth_layer)
            # 2-k ISTA steps
            for k in range(1, self.config['K']):
                h_t_kth_layer = self.soft_l1(torch.mm(h_t_last_layer, W_k.t()) + \
                                             torch.mm(input[t + 1], V.t()) + \
                                             torch.mm(h_t_kth_layer, S.t()),
                                             self.lambda0 / self.alpha)
                h[-1].append(h_t_kth_layer)

        h_stacked = [torch.stack(h[i]) for i in range(len(h))]
        self.sparse_code = torch.stack(h_stacked)

    def forward(self, raw_input):

        if self.config['model'].lower() in ['reweighted', 'sista', 'l1l1']:

            # Compression
            self.batch_size = raw_input.shape[1] # time_step, batch_size, data_dim
            raw_input_reshape = raw_input.view([-1, raw_input.shape[-1]])
            now_input_reshape = raw_input_reshape.mm(self.A.t())
            self.now_input = now_input_reshape.view([raw_input.shape[0], raw_input.shape[1], -1])

            pre_input = torch.zeros([1, self.now_input.shape[1], self.n_input], dtype=self.dtype, device=self.device)
            # Sista graph
            input = torch.cat([pre_input, self.now_input])

            if self.config['model'].lower() == 'reweighted':
                self.build_graph_reweighted(input)
            elif self.config['model'].lower() == 'sista':
                self.build_graph_sista(input)
            elif self.config['model'].lower() == 'l1l1':
                self.build_graph_l1_l1(input)

            zeros_count = torch.sum(torch.sum(torch.sum((self.sparse_code == 0).int(), dim=-1), dim=-1),
                                    dim=0).data.float()
            self.sparsity = zeros_count / (self.sparse_code.numel() / self.config['K'])
            # reconstruction
            sparse_code_reshape = self.sparse_code[:, -1, ...].contiguous().view([-1, self.config['n_hidden']])
            z_hat_flattened = torch.mm(sparse_code_reshape, self.D.t())
            z_hat = z_hat_flattened.view([self.config['time_steps'], self.batch_size, -1])
            return z_hat

        else:
            compressed = self.compression(raw_input)
            sparse_code = self.generic(compressed)[0]
            z_hat = self.reconstruction(sparse_code)
            return z_hat
