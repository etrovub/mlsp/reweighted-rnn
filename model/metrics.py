import torch
import numpy as np

def psnr(ref, reconstructed):
    '''if std is not None:
        ref *= std
        reconstructed *= std
    if mean is not None:
        ref += mean
        reconstructed += mean'''

    # ref *= 255.
    # reconstructed *= 255.

    reconstructed[reconstructed < 0] = 0
    reconstructed[reconstructed > 255] = 255
    reconstructed = torch.round(reconstructed)
    ref[ref < 0] = 0
    ref[ref > 255] = 255
    ref = torch.round(ref)
    mse = torch.mean((ref - reconstructed) ** 2)
    if mse == 0:
        return torch.tensor(100.0)
    return 20 * torch.log10(255. / torch.sqrt(mse))

def psnr_numpy(ref, reconstructed):
    '''if std is not None:
        ref *= std
        reconstructed *= std
    if mean is not None:
        ref += mean
        reconstructed += mean'''

    # ref *= 255.
    # reconstructed *= 255.

    reconstructed[reconstructed < 0] = 0
    reconstructed[reconstructed > 255] = 255
    reconstructed = np.round(reconstructed)
    ref[ref < 0] = 0
    ref[ref > 255] = 255
    ref = np.round(ref)
    mse = np.mean((ref - reconstructed) ** 2)
    if mse == 0:
        return 100
    return 20 * np.log10(255. / np.sqrt(mse))


def mse(ref, reconstructed):
    reconstructed[reconstructed < 0] = 0
    reconstructed[reconstructed > 255] = 255
    reconstructed = torch.round(reconstructed)
    mse = torch.mean((ref - reconstructed) ** 2)
    return mse