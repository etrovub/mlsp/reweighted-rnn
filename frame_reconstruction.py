import time

from dataloaders.data_loader import Moving_MNIST_Loader
from dataloaders.shanghaitech import ShanghaiTechLoader
from dataloaders.ucsd import UCSD_Loader
from dataloaders.yup import YUP_Loader
from model.model_reconstruction import Unfolding_RNN
from model import metrics
import numpy as np
from scipy.io import loadmat
import scipy.io as sio
import yaml
import argparse
from datetime import datetime
import os
from os.path import join
import torch
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import tqdm
import cv2
import gc

from dataloaders.utils import create_overlapping_patches, assemble_overlapping_patches, assemble_patches, create_patches


class Frame_Reconstruction():
    def __init__(self, configs):
        self.configs = configs
        self.dtype = torch.float32
        self.writer = open(join(config['log_folder'], 'output.txt'), 'w')

        if torch.cuda.is_available():
            self.device = torch.device('cuda')
        else:
            self.device = torch.device('cpu')

        if configs['model'].lower() in ['sista', 'l1l1', 'reweighted', 'gru', 'lstm', 'rnn']:
            D_init = loadmat(configs['D_init_file_path'])['dict'].astype('float32')
            n_input = int(configs['n_features'] / configs['compression_factor'])
            A_init = np.asarray(
                np.random.uniform(
                    low=-np.sqrt(6.0 / (n_input + configs['n_features'])),
                    high=np.sqrt(6.0 / (n_input + configs['n_features'])),
                    size=(n_input, config['n_features'])
                ) / 2.0, dtype=np.float32)
            self.model = Unfolding_RNN(A_init, D_init, configs).to(self.device)
        else:
            print('not recognised model, exitting...')
            exit()

        if configs['resume'] is True:
            assert os.path.exists(configs['checkpoint'])
            temp = torch.load(configs['checkpoint'])
            self.model.load_state_dict(torch.load(configs['checkpoint']))
            print('Checkpoint at {} successfully loaded.'.format(configs['checkpoint']))


    def compound_loss(self, ref, pre, sparse_codes, lb):

        loss = self.metric(ref, pre)
        l1loss = torch.mean(torch.norm(sparse_codes[:, -1, ...], p=1, dim=-1).flatten())
        return lb * l1loss + loss

    def train(self, data_loader, include_test=True):
        # if not data_loader.train.shape[1] % self.configs['batch_size'] == 0:
        #    raise NotImplementedError('for now, please choose the batch_size by which the number of training samples is divisible')

        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.configs['lr'],
                                          weight_decay=self.configs['weight_decay'])
        self.scheduler = ReduceLROnPlateau(self.optimizer, mode='min', factor=0.3, patience=5, verbose=True)
        self.metric = torch.nn.MSELoss()

        best_val_psnr = 0

        for epoch in range(1, self.configs['num_epoch'] + 1):

            print('Training epoch {}'.format(epoch))
            train_loss, train_psnr = self.train_one_epoch(data_loader)

            # Count parameters
            model_parameters = filter(lambda p: p.requires_grad, self.model.parameters())
            params = sum([np.prod(p.size()) for p in model_parameters])
            print('Current model has {} trainable parameters'.format(params))

            if train_loss > 1e8 or torch.isnan(torch.tensor(train_loss)):
                to_print = 'epoch {}: loss exploded'.format(epoch)
                print(to_print)
                self.writer.write(to_print)
                self.writer.close()
                exit()

            val_loss, val_psnr = self.validation(data_loader)

            # Model checkpoint
            if val_psnr > best_val_psnr:
                best_val_psnr = val_psnr
                torch.save(self.model.state_dict(), self.configs['checkpoint'])
                print('val_psnr increased -> Model checkpoint saved.')

            self.scheduler.step(val_loss)
            if epoch % self.configs['display_each'] == 0:
                to_print = 'epoch: {}, train loss: {}, train psnr: {}, val loss: {}, val psnr: {}'.format(epoch,
                                                                                                          train_loss,
                                                                                                          train_psnr,
                                                                                                          val_loss,
                                                                                                          val_psnr)
                if self.configs['model'].lower() in ['sista', 'l1l1', 'reweighted']:
                    to_print += '\nld0: {}, ld1: {}, ld2: {}, sparsity: {}, compressed input range: {} to {} '.format(
                        self.model.lambda0,
                        self.model.lambda1,
                        self.model.lambda2,
                        self.model.sparsity,
                        self.model.now_input.min(),
                        self.model.now_input.max())
                print(to_print)
                self.writer.write(to_print + '\n')

        # if self.configs['model'].lower() in ['rnn', 'reweighted', 'l1l1', 'sista']:
        #     self._save_network_statistics(data_loader)
        if include_test:
            test_loss, test_psnr = self.test(data_loader)

    def train_one_epoch(self, data_loader):
        psnr_batch = []
        loss_batch = []
        iterator = tqdm(range(int(data_loader.train.shape[1] / self.configs['batch_size'])))
        for i in iterator:
            batch = data_loader.load_batch_train(self.configs['batch_size'])
            input = torch.tensor(batch, dtype=self.dtype, device=self.device)
            output = self.model.forward(input)

            if self.configs['l0'] > 0:
                loss = self.compound_loss(input, output, self.model.sparse_code,
                                          torch.tensor(self.configs['l0']).cuda())
            else:
                loss = self.metric(input, output)

            if loss > 1e8:
                print('Loss exploded')

            iterator.set_description('batch loss: {:.6f}'.format(loss))

            self.optimizer.zero_grad()
            loss.backward()
            # if self.configs['model'].lower() in ['sista', 'reweighted', 'l1l1', 'lstmlstm']:
            if self.configs['gradient_clip'] is True:
                torch.nn.utils.clip_grad_norm_(self.model.parameters(), 0.25)
            self.optimizer.step()

            loss_batch.append(loss.item())
            psnr_batch.append(metrics.psnr(input, output).item())

        loss = sum(loss_batch) / len(loss_batch)
        psnr = sum(psnr_batch) / len(psnr_batch)

        return loss, psnr

    def save_network_statistics(self, data_loader):
        '''
        save network weights and sparse codes
        '''
        self.model.eval()
        sparse_code = np.empty([self.configs['time_steps'], data_loader.train.shape[1], self.configs['n_hidden']],
                                dtype=np.float32)
        with torch.no_grad():
            data_loader.current_idx_train = 0
            while data_loader.current_idx_train + self.configs['batch_size'] <= min(data_loader.train.shape[1], 1024):
                data_torch = torch.Tensor(data_loader.load_batch_train(self.configs['batch_size'])).to(self.device)
                output = self.model.forward(data_torch)
                if self.configs['model'].lower() == 'rnn':
                    sparse_code[:,
                    data_loader.current_idx_train: self.configs['batch_size'] + data_loader.current_idx_train,
                    :] = self.model.hidden.data.cpu().numpy()
                else:
                    sparse_code[:, :,
                    data_loader.current_idx_train: data_torch.shape[1] + data_loader.current_idx_train, :] \
                        = self.model.sparse_code.data.cpu().numpy()[:, :,
                          :min(data_torch.shape[1], data_loader.train.shape[1] - data_loader.current_idx_train,
                               1024 - data_loader.current_idx_train), ...]

        mat = {
            'sparse_codes': sparse_code,
            'D': self.model.D.data.cpu().numpy(),
            'A': self.model.A.data.cpu().numpy(),
            'h0': self.model.h_0.data.cpu().numpy(),
            'c': self.model.alpha.data.cpu().numpy(),
            'lambdas': np.array([self.model.lambda0.data.cpu().numpy(), self.model.lambda1.data.cpu().numpy(),
                                 self.model.lambda2.data.cpu().numpy()])
        }

        if self.configs['model'].lower() == 'reweighted':
            mat['G'] = self.model.G.data.cpu().numpy()
            mat['Z'] = self.model.Z.data.cpu().numpy()
            mat['g'] = self.model.g.data.cpu().numpy()
        elif self.configs['model'].lower() == 'l1l1':
            mat['G'] = self.model.G.data.cpu().numpy()
        else:
            mat['F'] = self.model.F.data.cpu().numpy()

        sio.savemat('statistics' + self.configs['model'] + '.mat', mat)


    def validation(self, data_loader):
        with torch.no_grad():
            psnr_batch = []
            loss_batch = []
            for i in tqdm(range(int(data_loader.eval.shape[1] / self.configs['batch_size']))):
                batch = data_loader.load_batch_validation(self.configs['batch_size'])
                input = torch.tensor(batch, dtype=self.dtype, device=self.device)
                output = self.model.forward(input)

                if self.configs['l0'] > 0:
                    loss = self.compound_loss(input, output, self.model.sparse_code,
                                              torch.tensor(self.configs['l0']).cuda())
                else:
                    loss = self.metric(input, output)

                loss_batch.append(loss.item())
                psnr_batch.append(metrics.psnr(input, output).item())

            loss = sum(loss_batch) / len(loss_batch)
            psnr = sum(psnr_batch) / len(psnr_batch)

            to_print = 'test_loss: {}, test_psnr: {}'.format(loss, psnr)
            print(to_print)
            self.writer.write(to_print + '\n')
            # self.writer.close()
        return loss, psnr

    def test(self, data_loader, output_mnist=False):
        with torch.no_grad():
            psnr_batch = []
            loss_batch = []
            reconstruct_to_file = []
            for i in tqdm(range(int(data_loader.test.shape[1] / self.configs['batch_size']))):
                test_batch = data_loader.load_batch_test(self.configs['batch_size'])
                input = torch.tensor(test_batch, dtype=self.dtype, device=self.device)
                output = self.model.forward(input)

                if self.configs['l0'] > 0:
                    loss = self.compound_loss(input, output, self.model.sparse_code,
                                              torch.tensor(self.configs['l0']).cuda())
                else:
                    loss = self.metric(input, output)

                loss_batch.append(loss.item())
                psnr_batch.append(metrics.psnr(input, output).item())

                if self.configs['dataset'] == 'moving_mnist' and output_mnist is True:
                    # Saving samples
                    output[output>255.0] = 255.0
                    output[output<0.0] = 0.0
                    sample_gt = np.squeeze(test_batch[10, 0, ...])
                    sample_out = np.squeeze(output[10, 0, ...].cpu().detach().numpy())
                    abs_error = np.abs(sample_out - sample_gt)

                    sample_gt = cv2.cvtColor(np.reshape(sample_gt, (16, 16)), cv2.COLOR_GRAY2BGR)
                    sample_out = cv2.cvtColor(np.reshape(sample_out, (16, 16)), cv2.COLOR_GRAY2BGR)

                    cv2.imwrite(os.path.join(self.configs['log_folder'], '{}_10_gt.png'.format(i)), sample_gt)
                    cv2.imwrite(os.path.join(self.configs['log_folder'], '{}_10_out.png'.format(i)), sample_out)

                    error_scale_factor = 4
                    abs_error_scaled = abs_error * error_scale_factor
                    abs_error_scaled[abs_error_scaled > 255.0] = 255.0
                    img_error = cv2.applyColorMap(np.reshape(abs_error_scaled.astype(np.uint8), (16,16)), cv2.COLORMAP_JET)
                    cv2.imwrite(os.path.join(self.configs['log_folder'], '{}_10_ae.png'.format(i)), img_error)

            loss = sum(loss_batch) / len(loss_batch)
            psnr = sum(psnr_batch) / len(psnr_batch)


        to_print = 'test_loss: {}, test_psnr: {}'.format(loss, psnr)
        print(to_print)
        self.writer.write(to_print + '\n')
        self.writer.close()
        return loss, psnr


    def test_video(self, data_loader, split, sample_output=[10], error_map=False):
        '''
        Frame reconstruction on the whole video using non-overlapping patches and sequences of time_steps frames
        If sample_output is not None, will save the [sample_output] frames in config['log_dir']
        '''

        with torch.no_grad():
            clips = data_loader.test_clips
            psnrs = []

            for c_i, clip in enumerate(clips):
                sx, sy = clip.shape[1], clip.shape[2]

                # Create patches
                # patches = create_overlapping_patches(clip, self.configs['tile_size'],
                #                                      int(self.configs['tile_size'] / 2))
                patches = create_patches(clip, self.configs['tile_size'])

                # Flatten and permute patches/frames indices
                patches = np.swapaxes(np.reshape(patches, (patches.shape[0], patches.shape[1], -1)), 0, 1)

                # Inference (with batch division to avoid memory OF)
                torch.cuda.empty_cache()
                output = torch.tensor(np.empty(patches.shape), dtype=self.dtype, device='cpu')

                batch_idx = 0
                while batch_idx < patches.shape[1]:
                    end_batch = min(batch_idx + self.configs['batch_size'], patches.shape[1])
                    patch_output = self.model.forward(
                        torch.tensor(patches[:, batch_idx:end_batch, ...], dtype=self.dtype,
                                     device=self.device))
                    output[:, batch_idx:end_batch, ...] = patch_output.cpu()

                    batch_idx += self.configs['batch_size']

                # Reconstruct output clip
                out = output.cpu().detach().numpy()
                out = np.reshape(out, (patches.shape[0], patches.shape[1], self.configs['tile_size'], self.configs['tile_size']))
                # out = assemble_overlapping_patches(out, self.configs['tile_size'], sx, sy,
                #                                    int(self.configs['tile_size'] / 2))
                out = assemble_patches(out, self.configs['tile_size'], sx, sy)

                out[out > 255.0] = 255.0
                out[out < 0.0] = 0.0
                psnrs.append(metrics.psnr_numpy(clip, out))
                #psnrs.append(metrics.psnr(torch.tensor(clip, dtype=self.dtype, device='cpu'), torch.tensor(out, device='cpu', dtype=self.dtype)).item())

                if sample_output is not None:
                    #if c_i%10 == 0:
                    for so in sample_output:
                        img_gt, img_pred = np.squeeze(clip[so, ...]), np.squeeze(out[so, ...])
                        abs_error = np.abs(img_pred - img_gt)
                        img_gt = cv2.cvtColor(img_gt, cv2.COLOR_GRAY2BGR)
                        img_pred = cv2.cvtColor(img_pred, cv2.COLOR_GRAY2BGR)
                        cv2.imwrite(os.path.join(self.configs['log_folder'], '{}_{}_gt.png'.format(c_i, so)), img_gt)
                        cv2.imwrite(os.path.join(self.configs['log_folder'], '{}_{}_rec.png'.format(c_i, so)), img_pred)

                        #Output error map
                        if error_map is True:
                            error_scale_factor = 4
                            abs_error_scaled = abs_error*error_scale_factor
                            abs_error_scaled[abs_error_scaled > 255.0] = 255.0
                            img_error = cv2.applyColorMap(abs_error_scaled.astype(np.uint8), cv2.COLORMAP_JET)
                            cv2.imwrite(os.path.join(self.configs['log_folder'], '{}_{}_ae.png'.format(c_i, so)), img_error)


            #Mean PSNR
            avg_psnr = sum(psnrs) / len(psnrs)
            print('Avg_psnr {}'.format(avg_psnr))



if __name__ == '__main__':
    CONFIG_PATH = 'frame_reconstruction_configs.yaml'
    with open(CONFIG_PATH, 'r') as stream:
        config = yaml.load(stream)
        for key, val in config.items():
            try:
                val = int(val)
            except:
                pass

    parser = argparse.ArgumentParser(description='Multiple L1 for frame reconstruction')
    parser.add_argument('-m', '--model')
    parser.add_argument('-ld0', '--lambda0')
    parser.add_argument('-ld1', '--lambda1')
    parser.add_argument('-ld2', '--lambda2')
    parser.add_argument('-learn_ld0', '--learn_lambda0')
    parser.add_argument('-learn_ld1', '--learn_lambda1')
    parser.add_argument('-learn_ld2', '--learn_lambda2')

    parser.add_argument('-k', '--num_layer')
    parser.add_argument('-wd', '--weight_decay')
    parser.add_argument('-lr', '--lr')
    parser.add_argument('-cf', '--compression_factor')
    parser.add_argument('-epoch', '--num_epoch')
    parser.add_argument('-nh', '--num_hidden', help='number of units per hidden layer')
    parser.add_argument('-ts', '--time_steps', help='number of time steps to load from each sequence')

    parser.add_argument('-d_path', '--dictionary_init_path')

    parser.add_argument('-d', '--dataset', choices=['moving_mnist', 'yup', 'ucsd', 'shanghaitech', 'avenue'],
                        default='moving_mnist')

    parser.add_argument('-r', '--resume', action='store_true', default=False)
    parser.add_argument('-cpt', '--checkpoint', default=None)
    parser.add_argument('-t', '--test', action='store_true', default=False)
    parser.add_argument('-tc', '--test_config')
    parser.add_argument('-gc', '--gradient_clip', action='store_true', default=False)
    parser.add_argument('-l0', '--l0_loss', default=0.0)
    parser.add_argument('-em', '--error_map', default=False, action='store_true')

    args = vars(parser.parse_args())
    if args['model']:
        config['model'] = args['model']
    if args['lambda0']:
        config['lambda0'] = float(args['lambda0'])
    if args['lambda1']:
        config['lambda1'] = float(args['lambda1'])
    if args['lambda2']:
        config['lambda2'] = float(args['lambda2'])
    if args['learn_lambda0']:
        config['learn_lambda0'] = int(args['learn_lambda0'])
    if args['learn_lambda1']:
        config['learn_lambda1'] = int(args['learn_lambda1'])
    if args['learn_lambda2']:
        config['learn_lambda2'] = int(args['learn_lambda2'])

    if args['compression_factor']:
        config['compression_factor'] = float(args['compression_factor'])
    if args['num_layer']:
        config['K'] = int(args['num_layer'])
    if args['weight_decay']:
        config['weight_decay'] = float(args['weight_decay'])
    if args['lr']:
        config['lr'] = float(args['lr'])
    if args['num_epoch']:
        config['num_epoch'] = int(args['num_epoch'])
    if args['num_hidden']:
        config['n_hidden'] = int(args['num_hidden'])
    if args['time_steps']:
        config['time_steps'] = int(args['time_steps'])
    if args['dictionary_init_path']:
        config['D_init_file_path'] = str(args['dictionary_init_path'])

    if args['checkpoint']:
        config['checkpoint'] = str(args['checkpoint'])
    config['resume'] = bool(args['resume'])
    config['test'] = bool(args['test'])
    if args['test_config']:
        config['test_config'] = str(args['test_config'])
    config['gradient_clip'] = bool(args['gradient_clip'])
    config['l0'] = float(args['l0_loss'])

    now = datetime.now()
    config['log_folder'] = join(config['log_path'], '{}_{}_{} {}h{}_{}'.format(now.year, now.month, now.day, now.hour, now.minute, now.second))

    config['dataset'] = str(args['dataset'])

    config['log_folder'] += ' ' + config['model'] + '_' + config['dataset'] + '_k' + str(config['K']) + '_C' + str(
        config['compression_factor']) + '_n' + str(config['n_hidden'])
    os.makedirs(config['log_folder'])

    if not os.path.exists(os.path.dirname(config['checkpoint'])):
        os.makedirs(os.path.dirname(config['checkpoint']))

    # save configs to file
    with open(join(config['log_folder'], 'configs.yml'), 'w') as outfile:
        yaml.dump(config, outfile, default_flow_style=False)

    if config['dataset'] == 'moving_mnist':
        data_loader = Moving_MNIST_Loader(config['moving_mnist_path'], time_steps=config['time_steps'], load_only=-1,
                                          flatten=True, scale=False)
    elif config['dataset'] == 'yup':
        data_loader = YUP_Loader(config['yup_path'], patch_size=config['tile_size'], time_steps=20, flatten=True,
                                 scale=False, normalize=False, category=['Street'], camera=['static'])

    elif config['dataset'] == 'ucsd':
        data_loader = UCSD_Loader(config['ucsd_path'], patch_size=config['tile_size'], time_steps=20, flatten=True,
                                  scale=False)

    elif config['dataset'] == 'shanghaitech':
        data_loader = ShanghaiTechLoader(config['shanghaitech_path'], patch_size=config['tile_size'], time_steps=20,
                                         flatten=True, scale=False)

    elif config['dataset'] == 'avenue':
        data_loader = ShanghaiTechLoader(config['shanghaitech_path'], patch_size=config['tile_size'], time_steps=20,
                                         flatten=True, scale=False, split=['avenue'])

    else:
        print(config['dataset'] + ': Invalid dataset')
        exit()

    np.random.seed(config['seed'])
    torch.manual_seed(config['seed'])
    fr = Frame_Reconstruction(config)

    if config['test'] is False:
        fr.train(data_loader)
    else:
        if config['test_config'] == 'video_test':
            fr.test_video(data_loader, 'test', error_map=args['error_map'])
        elif config['test_config'] == 'video_eval':
            fr.test_video(data_loader, 'eval', error_map=args['error_map'])
        elif config['test_config'] == 'patch_test':
            fr.metric = torch.nn.MSELoss()
            fr.test(data_loader)
        elif config['test_config'] == 'patch_eval':
            fr.metric = torch.nn.MSELoss()
            fr.validation(data_loader)
        elif config['test_config'] == 'mnist_eval':
            fr.metric = torch.nn.MSELoss()
            fr.validation(data_loader)
        elif config['test_config'] == 'mnist_test':
            fr.metric = torch.nn.MSELoss()
            fr.test(data_loader, output_mnist=False)
        else:
            print('Unrecognized test configuration')
