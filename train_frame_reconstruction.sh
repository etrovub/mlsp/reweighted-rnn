python frame_reconstruction.py -m reweighted -ld0 0.3 -ld1 0.02 -ld2 0.0 -learn_ld0 1 -learn_ld1 1 -learn_ld2 0 -k 3 -cf 5 -d moving_mnist -cpt checkpoints/checkpoint_reweighted.pth -epoch 200 -lr 0.0003 -gc

python frame_reconstruction.py -m reweighted -ld0 0.3 -ld1 0.02 -ld2 0.0 -learn_ld0 1 -learn_ld1 1 -learn_ld2 0 -k 3 -cf 5 -d moving_mnist -cpt checkpoints/checkpoint_l1l1.pth -epoch 200 -lr 0.0003 -gc

python frame_reconstruction.py -m reweighted -ld0 2.0 -ld1 0 -ld2 0.2 -learn_ld0 1 -learn_ld1 0 -learn_ld2 1 -k 3 -cf 5 -d moving_mnist -cpt checkpoints/checkpoint_sista.pth -epoch 200 -lr 0.0003 -gc
