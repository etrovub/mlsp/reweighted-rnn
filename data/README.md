### Download intialized DCT dictionnaries

Pre-initialized dictionnaries can be found in:
https://drive.google.com/drive/folders/1EoTINzi6uiZZIMRhvm2x2dp7j81VDrB_?usp=sharing

Alternatively, yoiu can use dctmtx1.m to generate your own.

### Preprocessed moving MNIST

This can be downloaded from https://drive.google.com/drive/folders/1L5HJqCt2HfNJk_pqbf7Qx3aKvlRbd2y0?usp=sharing

Alternatively, you can download it on http://www.cs.toronto.edu/~nitish/unsupervised_video/ and use scale_data.py to preprocess the dataset.

### Real video datasets preprocessing

For the real-video datasets, this can be done using preprocess.py (resizing and grayscaling) or similar script.
