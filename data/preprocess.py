import cv2
import numpy as np
import os
import matplotlib.pyplot as plt

frames_folders = ['avenue/training/frames', 'avenue/testing/frames', 'shanghaitech/testing/frames']
videos_folders = ['shanghaitech/training/videos']
preprocessed_folder = 'preprocessed'

#UCSD 238x158 px
target_height = 160
'''
#frames
for folder in frames_folders:

    subfolders = os.listdir(folder)
    for subfolder in subfolders:
        sf = os.path.join(folder, subfolder)
        of = os.path.join(preprocessed_folder, folder, subfolder)

        if os.path.exists(of) is False:
            os.makedirs(of)

        frames = os.listdir(sf)
        print(subfolder)
        for f in frames:

            outputfile = os.path.join(of, f.split('.')[0])+'.png'

            image = cv2.cvtColor(cv2.imread(os.path.join(sf, f)), cv2.COLOR_RGB2GRAY)
            sx, sy = image.shape[1], image.shape[0]
            nsx = np.round(sx * target_height/sy).astype('int')
            image = cv2.resize(image, (nsx, target_height), interpolation=cv2.INTER_AREA)

            if os.path.exists(outputfile) is False:
                cv2.imwrite(outputfile, image)
'''

#videos
for folder in videos_folders:

    videos = os.listdir(folder)
    for video in videos:

        videofile = os.path.join(folder, video)
        outputfolder = os.path.join(preprocessed_folder, folder, video.split('.')[0])

        if os.path.exists(outputfolder) is False:
            os.makedirs(outputfolder)

        idx = 0
        vc = cv2.VideoCapture(videofile)
        ret, frame = vc.read()

        print(video)

        while ret is True:

            image = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            sx, sy = image.shape[1], image.shape[0]
            nsx = np.round(sx * target_height / sy).astype('int')
            image = cv2.resize(image, (nsx, target_height), interpolation=cv2.INTER_AREA)

            cv2.imwrite(os.path.join(outputfolder, '{:03d}.png'.format(idx)), image)

            idx +=1
            ret, frame = vc.read()
