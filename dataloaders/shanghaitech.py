import os
import cv2
import numpy as np
from dataloaders.utils import divide_sequence, create_patches

def retreive_files(basedir, maxframes=1000):
    out = []
    sequences = os.listdir(basedir)
    for s in sequences:
        out.append([])
        for i in range(1, maxframes+1):
            filename = os.path.join(basedir, s, '{:03d}.tif'.format(i))
            if os.path.exists(filename):
                out[-1].append(filename)
            else:
                filename = os.path.join(basedir, s, '{:03d}.png'.format(i))
                if os.path.exists(filename):
                    out[-1].append(filename)
                else:
                    filename = os.path.join(basedir, s, '{:04d}.png'.format(i))
                    if os.path.exists(filename):
                        out[-1].append(filename)
                    else:
                        filename = os.path.join(basedir, s, '{:02d}.png'.format(i))
                        if os.path.exists(filename):
                            out[-1].append(filename)

    return out


def load_frames(files, patch_size, scale):
    dest = []
    for s in files:
        frames = []

        # Preprocessing frames
        for fr in s:

            f = cv2.imread(fr, cv2.IMREAD_GRAYSCALE)

            # Cropping to bottom left
            sx, sy = f.shape[0], f.shape[1]
            nsx = sx - (sx % patch_size)
            nsy = sy - (sy % patch_size)
            f = f[sx - nsx:, :nsy]

            # Scaling
            if scale is True:
                f = f / 255.0

            frames.append(f)

        dest.append(np.array(frames))

    return dest



class ShanghaiTechLoader():

    '''
    Loader for Shanghaitech
    Images should be already resized (use preprocessed folder)
    Do not contain the Subway scenes
    Train/eval split: approximately 80-20. We take a balanced fraction of video samples per category
    '''

    def __init__(self, path, patch_size=16, time_steps=20, flatten=True, scale=False, normalize=True, split=None):

        self.patch_size = patch_size
        self.time_steps = time_steps
        self.scale = scale

        MAXFRAMES=200

        #folders = ['avenue', 'ped1', 'ped2', 'shanghaitech']
        folders = ['shanghaitech']
        if split is not None:
            folders = split
        train_folder = 'training/frames'
        test_folder = 'testing/frames'

        train_files = []
        test_files = []

        for folder in folders:
            train_dir = os.path.join(path, folder, train_folder)
            test_dir = os.path.join(path, folder, test_folder)

            train_files.extend(retreive_files(train_dir, MAXFRAMES))
            test_files.extend(retreive_files(test_dir, MAXFRAMES))

        # Select couple of evaluation videos
        self.eval_idx = list(range(0, len(train_files), 5))
        self.eval_files = [train_files[i] for i in self.eval_idx]
        self.train_files = [train_files[i] for i in range(len(train_files)) if i not in self.eval_idx]
        self.test_files = test_files

        #Loading frames
        self.train_clips = load_frames(self.train_files, patch_size, scale)
        self.eval_clips = load_frames(self.eval_files, patch_size, scale)
        self.test_clips = load_frames(self.test_files, patch_size, scale)

        print('Videos loaded: {} train, {} eval, {} test'.format(len(self.train_clips), len(self.eval_clips), len(self.test_clips)))

        #Dividing videos into clips
        self.train_clips = [divide_sequence(c, time_steps) for c in self.train_clips]
        self.train_clips = [item for sublist in self.train_clips for item in sublist]
        self.eval_clips = [divide_sequence(c, time_steps) for c in self.eval_clips]
        self.eval_clips = [item for sublist in self.eval_clips for item in sublist]
        self.test_clips = [divide_sequence(c, time_steps) for c in self.test_clips]
        self.test_clips = [item for sublist in self.test_clips for item in sublist]

        # Generate image patches for training and testing
        self.train_patches = np.concatenate([create_patches(c, patch_size) for c in self.train_clips])
        self.eval_patches = np.concatenate([create_patches(c, patch_size) for c in self.eval_clips])
        self.test_patches = np.concatenate([create_patches(c, patch_size) for c in self.test_clips])

        self.train_num, self.eval_num, self.test_num = self.train_patches.shape[0], self.eval_patches.shape[0], \
                                                       self.test_patches.shape[0]
        print(
            'Data formatted to patches. Number of {} frame long patches: {} train, {} eval, {} test'.format(time_steps,
                                                                                                            self.train_num,
                                                                                                            self.eval_num,
                                                                                                            self.test_num))
        # Flatten time sequences
        if flatten:
            self.train_patches = self.train_patches.reshape([self.train_num, time_steps, -1])
            self.eval_patches = self.eval_patches.reshape([self.eval_num, time_steps, -1])
            self.test_patches = self.test_patches.reshape([self.test_num, time_steps, -1])

        # Permute samples /frames indices
        self.train_patches = np.swapaxes(self.train_patches, 0, 1)
        self.eval_patches = np.swapaxes(self.eval_patches, 0, 1)
        self.test_patches = np.swapaxes(self.test_patches, 0, 1)

        self.current_idx_train = 0
        self.current_idx_eval = 0
        self.current_idx_test = 0

        self.shuffle()

    def load_videos_as_arrays(self, split):
        files = None
        videos = None
        if split == 'eval':
            files = [str(i) for i in self.eval_idx]
            videos = load_frames(self.eval_files, self.patch_size, self.scale)
        elif split == 'test':
            files = [str(i) for i in range(len(self.test_files))]
            videos = load_frames(self.test_files, self.patch_size, self.scale)
        return videos, files

    @property
    def train(self):
        return self.train_patches

    @property
    def eval(self):
        return self.eval_patches

    @property
    def test(self):
        return self.test_patches

    @property
    def eval_f(self):
        return self.eval_files

    @property
    def test_f(self):
        return self.test_files

    def reset_indices(self):
        self.current_idx_test, self.current_idx_eval, self.current_idx_train = 0, 0, 0

    def shuffle(self):
        '''
        Shuffle training set (video patches, across all clips)
        '''
        np.random.seed(2021)
        indices = np.random.permutation(self.train_patches.shape[1])
        self.train_patches = self.train_patches[:, indices, ...]
        self.current_idx_train = 0

    def load_batch_train(self, batch_size):
        if self.current_idx_train + batch_size >= self.train_num:
            batch_end = self.train_patches[:, self.current_idx_train:, ...]
            batch_start = self.train_patches[:, 0:batch_size - batch_end.shape[1], ...]
            batch = np.concatenate((batch_end, batch_start), axis=1)
            self.current_idx_train = 0
        else:
            batch = self.train_patches[:, self.current_idx_train:self.current_idx_train + batch_size, ...]
            self.current_idx_train += batch_size
        return batch

    def load_batch_validation(self, batch_size):
        if self.current_idx_eval + batch_size >= self.eval_num:
            batch = self.eval_patches[:, self.current_idx_eval:, ...]
            self.current_idx_eval = 0
        else:
            batch = self.eval_patches[:, self.current_idx_eval: self.current_idx_eval + batch_size, ...]
            self.current_idx_eval += batch_size
        return batch

    def load_batch_test(self, batch_size):
        if self.current_idx_eval + batch_size >= self.test_num:
            batch = self.test_patches[:, self.current_idx_test:, ...]
            self.current_idx_test = 0
        else:
            batch = self.test_patches[:, self.current_idx_test: self.current_idx_test + batch_size, ...]
            self.current_idx_test += batch_size
        return batch


if __name__ == '__main__':
    path = '../../Shanghaitech/preprocessed/'
    loader = ShanghaiTechLoader(path, flatten=False, scale=False, split=['avenue'])