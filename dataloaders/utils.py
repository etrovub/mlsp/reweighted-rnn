import numpy as np


def divide_sequence(source, time_steps):
    seq_num = int(len(source) / time_steps)
    return [source[time_steps * i:time_steps * (i + 1), :, :] for i in range(seq_num)]


def create_patches(source, patch_size):
    duration = source.shape[0]
    sx, sy = source.shape[1], source.shape[2]
    assert(sx % patch_size == 0)
    assert(sy % patch_size == 0)
    nx, ny = int(sx/patch_size), int(sy/patch_size)

    out = np.empty((nx*ny, duration, patch_size, patch_size), dtype='float32')
    for i in range(nx):
        for j in range(ny):
            out[i*ny + j, ...] = source[:, i*patch_size:(i+1)*patch_size, j*patch_size:(j+1)*patch_size]

    return out


def create_overlapping_patches(source, patch_size, stride):
    duration = source.shape[0]
    sx, sy = source.shape[1], source.shape[2]
    assert(stride <= patch_size)
    #Check if image fits the patches
    assert((sx - patch_size) % stride == 0)
    assert((sy - patch_size) % stride == 0)

    ny = int((sy - patch_size)/stride + 1)
    nx = int((sx - patch_size)/stride + 1)

    #extract patches
    out = np.empty((nx*ny, duration, patch_size, patch_size), dtype='float32')
    for i in range(nx):
        for j in range(ny):
            out[i*ny + j, ...] = source[:, i*stride:i*stride + patch_size, j*stride:j*stride + patch_size]

    return out


def assemble_patches(source, patch_size, sx, sy, overlap=0):
    if overlap==0:
        assert (sx % patch_size == 0)
        assert (sy % patch_size == 0)
        nx, ny = int(sx / patch_size), int(sy / patch_size)
        timesteps = source.shape[0]

        output = np.empty((timesteps, sx, sy), dtype='float32')
        for i in range(nx):
            for j in range(ny):
                for t in range(timesteps):
                    output[t, i*patch_size:(i+1)*patch_size, j*patch_size:(j+1)*patch_size] = source[t, i*ny + j, ...]

        return output

    else:
        raise Exception('Overlap !=0 not yet implemented')


def assemble_overlapping_patches(source, patch_size, sx, sy, stride):
    # Check if image fits the patches
    assert ((sx - patch_size) % stride == 0)
    assert ((sy - patch_size) % stride == 0)
    ny = int((sy - patch_size)/stride + 1)
    nx = int((sx - patch_size)/stride + 1)
    timesteps = source.shape[0]

    output = np.zeros((timesteps, sx, sy), dtype='float32')
    avmat = np.zeros((sx, sy), dtype='float32')
    for i in range(nx):
        for j in range(ny):
            for t in range(timesteps):
                output[t, i*stride:i*stride + patch_size, j*stride:j*stride+patch_size] += source[t, i*ny + j, ...]
            avmat[i*stride:i*stride + patch_size, j*stride:j*stride+patch_size] += 1
    output = output / avmat
    return output