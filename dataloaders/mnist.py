import torch
from torchvision import datasets, transforms
import torch.nn as nn


def generate_data(root, batch_size):
    train_set = datasets.MNIST(root=root, train=True, download=True,
                               transform=transforms.Compose([
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.1307,), (0.3081,)),
                               ]))
    test_set = datasets.MNIST(root=root, train=False, download=True,
                              transform=transforms.Compose([
                                  transforms.ToTensor(),
                                  transforms.Normalize((0.1307,), (0.3081,))
                              ]))

    train_set_part = torch.utils.data.dataset.Subset(train_set, list(range(0,50000)))
    eval_set_part = torch.utils.data.dataset.Subset(train_set, list(range(50000, 60000)))
    #[train_set_part, eval_set_part] = torch.utils.data.random_split(train_set, [50000, 10000])

    train_loader = torch.utils.data.DataLoader(train_set_part, batch_size=batch_size, shuffle=False, num_workers=4)
    eval_loader = torch.utils.data.DataLoader(eval_set_part, batch_size=batch_size, num_workers=2)
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size, num_workers=2)
    return train_loader, eval_loader, test_loader


def extract_patches(img, patch_shape, step=[1.0, 1.0]):
    patch_H, patch_W = patch_shape[0], patch_shape[1]

    if (img.size(2) < patch_H):
        num_padded_H_Top = (patch_H - img.size(2))//2
        num_paded_H_Bottom = patch_H - img.size(2) - num_padded_H_Top
        padding_H = nn.ConstantPad2d((0, 0, num_padded_H_Top, num_paded_H_Bottom), 0)
        img = padding_H(img)

    if (img.size(3) < patch_W):
        num_padded_W_Left = (patch_W - img.size(3))//2
        num_paded_W_Right = patch_W - img.size(3) - num_padded_W_Left
        padding_W = nn.ConstantPad2d((0, 0, num_padded_W_Left, num_paded_W_Right), 0)
        img = padding_W(img)

    step_int = [0, 0]
    step_int[0] = int(patch_H*step[0]) if (isinstance(step[0], float)) else step[0]
    step_int[1] = int(patch_W*step[1]) if (isinstance(step[1], float)) else step[1]

    patches_fold_H = img.unfold(2, patch_H, step_int[0])
    if(img.size(2) - patch_H) % step_int[0] != 0:
        patches_fold_H = torch.cat((patches_fold_H, img[:, :, -patch_H:, :].permute(0, 1, 3, 2).unsqueeze(2)), dim=2)
    patches_fold_HW = patches_fold_H.unfold(3, patch_W, step_int[1])
    if(img.size(2) - patch_W) % step_int[1] != 0:
        patches_fold_HW = torch.cat((patches_fold_HW, patches_fold_H[:, :, :, -patch_W:, :].permute(0, 1, 2, 4, 3).unsqueeze(3)), dim=3)
    patches = patches_fold_HW.permute(2, 3, 0, 1, 4, 5)
    patches = patches.reshape(-1, img.size(0), img.size(1), patch_H, patch_W)

    return patches
