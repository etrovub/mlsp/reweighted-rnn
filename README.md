Code for our paper: "Designing Interpretable Recurrent Neural Networks for Video Reconstruction Via Deep Unfolding"

> Van Luong, H., Joukovsky, B., & Deligiannis, N. (2021). Designing Interpretable Recurrent Neural Networks for Video Reconstruction via Deep Unfolding. IEEE Transactions on Image Processing, 30, 4099-4113.

### Structures
- Model.py: Includes the implementations of three unfolding models: SISTA-RNN, l1-l1 RNN, and our proposed model reweighted-RNN
- frame_reconstruction.py: To run the frame reconstruction experiments, calling the models from model.py
- run_frame_reconstruction.sh: Example scripts to run
- test_frame_reconstruction.sh: Example script to test model
- frame_reconstruction_configs.yaml, data_loader.py: Self-explanatory
- Data: Inlude the dictionary for models with 1024 hidden neurons, downloaded data should be put here

### Pre-requisites
- Python 3.6 or above (other versions have not been tested, but might still be ok)
- Dependencies: PyTorch (tested with 1.4.0), torchvision, pyyaml, argparse, opencv, tqdm, scipy, numpy

### Data
We included the data used for our experiments in data/moving_mnist. Alternatively, you can download the Moving MNIST dataset at http://www.cs.toronto.edu/~nitish/unsupervised_video/ and downscale frames to 16x16 (bilinear interpolation).

The Shanghaitech dataset (and UCSD, CUHK Avenue) can be downoaded from https://svip-lab.github.io/dataset/campus_dataset.html. Pre-processing is required (using data/preprocess.py) to replicate our results.

For dictionaries, they are to be placed in the 'data' folder. Download link:
https://drive.google.com/drive/folders/1EoTINzi6uiZZIMRhvm2x2dp7j81VDrB_?usp=sharing

### Running the experiments
This repository includes the implementations of three unfolding models: SISTA-RNN, l1-l1 RNN, and our proposed model reweighted-RNN. Please make sure the configurations are correct (pay attention to file paths).

run_frame_reconstruction.sh provides three configurations for sista-rnn, l1-l1-rnn and reweighted-RNN. For that, run the following:

`sh train_frame_reconstruction.sh`

Training with GPUs is encouraged.

### Testing the experiments

The following script can be used to test a model based on an existing checkpoint:

`sh test_frame_reconstruction.sh`

Note the following options in the test script file:
- -r resumes checkpoint
- -t test model
- -tc test configuration (choose mnist_eval, mnist_test, patch_eval, patch_test, video_eval, video_test) depending on the kind of dataset used.
- -em output video reconstruction error map
